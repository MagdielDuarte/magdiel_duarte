var path = require('path');

module.exports = {
  entry: './app/assets/scripts/index.js',
  module: {
    rules: [
      {
        test: /^(?!.*\.spec\.js$).*\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      }
    ]
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build/assets/scripts')
  }
};
