import Events from '../services/Events';

Events.register("jsListActions", "click", function(event){
  event.preventDefault();
  this.nextSibling
    .nextSibling
    .setAttribute('class', 'modal active');
});

Events.register("jsCloseModal", "click", function(event){
  event.preventDefault();
  this.parentElement
    .parentElement
    .parentElement
    .setAttribute('class', 'modal');
});

Events.register("modal", "click", function(event){
  event.preventDefault();
});

Events.register("js-profile__edit--btn", "click", function(event){
  event.preventDefault();

  const actions = document.getElementById('js-edit-actions');
  actions.classList.add('showing');

  this.classList.remove('show-on-mobile');
  this.classList.add('show-on-desktop');

  const editList = document.getElementById('js-profile__edit__list');
  editList.classList.add('hide');

  const editForm = document.getElementById('js-profile__edit__form');
  editForm.classList.add('showing');
});

Events.register("js-edit-cancel", "click", function(event){
  event.preventDefault();

  const actions = document.getElementById('js-edit-actions');
  actions.classList.remove('showing');

  const editBtn = document.querySelector('.js-profile__edit--btn');
  editBtn.classList.add('show-on-mobile');
  editBtn.classList.remove('show-on-desktop');

  const editList = document.getElementById('js-profile__edit__list');
  editList.classList.remove('hide');

  const editForm = document.getElementById('js-profile__edit__form');
  editForm.classList.remove('showing');
});
