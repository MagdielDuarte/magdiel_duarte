import WebFont from 'webfontloader';

WebFont.load({
   google: {
     families: ['Open Sans']
   },
   custom: {
    families: ['Ionicons'],
    urls: ['http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css']
  }
 });
