class Events {
  register(className, eventType, cb) {
    var elements = document.getElementsByClassName(className);

    for(let i=0; i < elements.length; i++) {
      elements[i]
        .addEventListener(eventType, cb);
    }
  }
}

export default new Events();
